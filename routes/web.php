<?php

use App\Http\Controllers\PayController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.accueil');
});
 Route::get('/pays', [PayController::class,'index'])->name("pays.index");
 Route::get('/pays/create', [PayController::class, 'create'])->name("pays.create");
 Route::post('/pays', [PayController::class, 'store'])->name('pays.store');



