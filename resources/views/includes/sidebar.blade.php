

<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset('../assets/img/sidebar-1.jpg')}}">

    <div class="logo"><a href="{{ url('/') }}" class="simple-text logo-normal">
         Creative Pays
      </a></div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item active  ">
          <a class="nav-link" href="{{ url("/pays") }}">
            <i class="material-icons">dashboard</i>
            <p>Table pays</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ url("/pays/create")}}">
            <i class="material-icons">table</i>
            <p>Ajouter</p>
          </a>
        </li>
      </ul>
    </div>
  </div>
