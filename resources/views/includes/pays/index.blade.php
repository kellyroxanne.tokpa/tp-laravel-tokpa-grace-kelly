@extends('layouts.main')

@section('content')

<div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title ">Table des pays</h4>
        <p class="card-category"> Ci dessous presenté la liste des differents pays </p>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class=" text-primary">

              <tr><th>
                ID
              </th>
              <th>
                Libelle
              </th>
              <th>
                Description
              </th>
              <th>
                Code_indicatif
              </th>
              <th>
                Continent
              </th>
              <th>
                Population
              </th>
              <th>
                Capitale
              </th>
              <th>
                Monnaie
              </th>
              <th>
                Langue
              </th>
              <th>
                Superficie
              </th>
              <th>
                Est_laique
              </th>


            </tr></thead>
            <tbody>
                @foreach ($pays as $pay)
                <tr>
                    <td>{{$pay->id}}</td>
                    <td>{{$pay->libelle}}</td>
                    <td>{{$pay->description}}</td>
                    <td><span class="tag tag-success">{{$pay->code_indicatif}}</span></td>
                    <td>{{$pay->continent}}</td>
                    <td>{{$pay->population}}</td>
                    <td>{{$pay->capitale}}</td>
                    <td>{{$pay->monnaie}}</td>
                    <td>{{$pay->langue}}</td>
                    <td>{{$pay->superficie}}</td>
                    <td>{{$pay->est_laique}}</td>
                  </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @endsection
