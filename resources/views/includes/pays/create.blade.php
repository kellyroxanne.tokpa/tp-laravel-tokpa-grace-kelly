@extends('layouts.main')

@section('content')

<div class="col-md-9" >
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Ajouter un pays</h4>
        <p class="card-category">Veuillez completer les champs de ce formulaire</p>
      </div>
      <form class="form-horizontal" action="{{route('pays.store')}}" method="POST" >
        @method("POST")
        @csrf
        <div class="card-body">
            <form>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Libelle</label>
                    <input type="text" name="libelle" class="form-control" >
                </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Code_indicatif</label>
                    <input type="text" name="code_indicatif" class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Description</label>
                    <input type="text" name="description" class="form-control">
                </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Continent</label>
                    <input type="text" name="continent" class="form-control">
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Population</label>
                    <input type="number" name="population" class="form-control">
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Capitale</label>
                    <input type="text" name="capitale" class="form-control">
                </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Superficie</label>
                    <input type="number" name="superficie" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                <div class="form-group bmd-form-group">
                    <select name="monnaie" id="" class="form-control">
                        <option value="XOF">XOF</option>
                        <option value="EUR">EUR</option>
                        <option value="DOLLAR">DOLLAR</option>
                    </select>
                </div>
                </div>
                <div class="col-md-4">
                <div class="form-group bmd-form-group">
                    <!--<label class="bmd-label-floating"> Langue</label>
                    <input type="text" class="form-control" >-->
                    <select name="langue" id="" class="form-control" >
                        <option value="FR">FR</option>
                        <option value="EN">EN</option>
                        <option value="ES">ES</option>
                        <option value="AR">AR</option>
                    </select>
                </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating">Est_laique</label>
                    <input type="text" name="est_laique" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
            <button type="submit" class="btn btn-primary pull-right">Ajouter</button>
            <div class="clearfix"></div>
            </form>
        </div>
      </form>
    </div>
  </div>
  @endsection

